#!/bin/sh

if [ $1 = "start" ]
then
    ps -p `cat server/server.pid` > /dev/null 2>&1
    if ! [ $? = "0" ]
    then
        echo "Starting local server"
        pushd server
        if (which python3 > /dev/null 2>&1)
        then
            python3 -m http.server 8000 > server.log 2>&1 &
        else
            python2 -m python -m SimpleHTTPServer 8000 > server.log 2>&1 &
        fi
        echo $! > server.pid
        popd
    else
    echo "Local server already running, PID" `cat server/server.pid`
    fi
elif [ $1 = "stop" ]
then
    ps -p `cat server/server.pid` > /dev/null 2>&1
    if [ $? = "0" ]
    then
	echo "Killing local server with PID" `cat server/server.pid`
	kill `cat server/server.pid`
    else
	echo "Local server not running"
    fi
else
    echo "Invalid command specified $1"
    echo "Usage: $0 <start|stop>"
fi
