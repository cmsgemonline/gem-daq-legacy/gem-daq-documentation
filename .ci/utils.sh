## Utility functions

authenticateKRB () {
    local SERVICE_ACCOUNT_PASSWORD=$(echo ${SERVICE_ACCOUNT_PASSWORD} | base64 -d)
    echo ${SERVICE_ACCOUNT_PASSWORD} | kinit -A -f ${SERVICE_ACCOUNT_USERNAME}@CERN.CH

    local SSHHOME=/tmp/.ssh
    mkdir -p ${SSHHOME}/tmp
    chmod go-rwx -R ${SSHHOME}
    touch ${SSHHOME}/config
    chmod go-rw ${SSHHOME}/config
    cat<<EOF>${SSHHOME}/config
ControlMaster             auto
ControlPersist            1000
ControlPath               ${SSHHOME}/tmp/master_%l_%h_%p_%r
ServerAliveInterval       30
HashKnownHosts            yes
StrictHostKeyChecking     no
GSSAPIAuthentication      yes
GSSAPITrustDNS            yes
GSSAPIDelegateCredentials yes
EOF
}

unauthenticateKRB () {
    kdestroy
    local SSHHOME=/tmp/.ssh
    if [ -d ${SSHHOME} ]
    then
        find ${SSHHOME} -type f -print0 -exec shred -n100 -u {} \;
        rm -rf ${SSHHOME}
    fi
}
