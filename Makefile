GUIDES = devguide \
         userguide \
         expertguide

KRB_USERNAME?=$(USER)
EOS_SITE_NAME?=cmsgemdaq
EOS_SITE_PATH?=/tmp/$(USER)/$(EOS_SITE_NAME)
SERVER_DIR?=server
DEPLOY_DIR?=$(SERVER_DIR)/$(EOS_SITE_NAME)

export EOS_SITE_URL?=https://$(EOS_SITE_NAME).web.cern.ch/$(EOS_SITE_NAME)
export GEM_DOCS_URL?=http://0.0.0.0:8000/$(EOS_SITE_NAME)

export LAST_UPDATE_DATE?=$(shell date -u +'%a %b %d %Y %T %Z')
export GIT_REVISION?=$(shell git describe --abbrev=8 --dirty --always)

GUIDES.CLEAN := $(patsubst %,%.clean, $(GUIDES))
GUIDES.RELINK:= $(patsubst %,%.relink, $(GUIDES))

.PHONY: $(GUIDES) $(GUIDES.CLEAN)
.PHONY: all api guides site relink
.PHONY: clean cleanapi cleansite
.PHONY: dummyserverstart dummyserverstop

## @pertarget default target generates guide docs
guides: $(GUIDES)

## @common generate guides and site
all: guides relink site

## @common create site structure for $(EOS_SITE_URL)/$(EOS_SITE_NAME)
site:
	make -C $@

## @helpers start local http server for testing cross-references and setting up site for deployment
dummyserverstart:
	mkdir -p $(DEPLOY_DIR)
	./server.sh start

## @helpers stop local http server for testing cross-references
dummyserverstop:
	./server.sh stop

## @helpers generate api docs (if packages are checked out in the api subdirectory)
api:
	make -C $@

## @pertarget second pass over local site to get cross-references correct
relink: $(GUIDES.RELINK)

## @common deploy docs to website hosted at $(EOS_SITE_PATH)
deploy: relink
	pushd server/$(EOS_SITE_NAME) && \
	rsync -e "ssh -F ${SSHHOME}/config" -ahvc --info=progress2 --partial --delete \
	    ./{*.html,guides,css,scripts} \
	    $(SERVICE_ACCOUNT_USERNAME)@lxplus.cern.ch:$(EOS_SITE_PATH)/

## @pertarget clean guides targets, stop local server
clean: $(GUIDES.CLEAN) dummyserverstop

## @common remove local site files
cleansite:
	rm -rf $(DEPLOY_DIR)

## build the docs, copy artifacts to local server
$(GUIDES):
	make -C $@
	mkdir -p $(DEPLOY_DIR)/guides
	rsync -ahc --delete --info=progress2 --partial $@/docs/build/html/ $(DEPLOY_DIR)/guides/$@
	@find $(DEPLOY_DIR)/guides/$@ -type f -iname '*.html' -print0 -exec \
		perl -pi -e "s|SITE_ROOT|$(EOS_SITE_NAME)|g" {} \+
	@find $(DEPLOY_DIR)/guides/$@ -type f -iname '*.html' -print0 -exec \
		perl -pi -e "s|http://0.0.0.0:8000/|/|g" {} \+
	@find $(DEPLOY_DIR)/guides/$@ -type f -iname '*.html' -print0 -exec \
		perl -pi -e "s|http://0.0.0.0:8000|/|g" {} \+
	@find $(DEPLOY_DIR)/guides/$@ -type f -iname '*.html' -print0 -exec \
		perl -pi -e "s|UPDATE_DATE|$(LAST_UPDATE_DATE)|g" {} \+
	@find $(DEPLOY_DIR)/guides/$@ -type f -iname '*.html' -print0 -exec \
		perl -pi -e "s|GIT_REVISION|$(GIT_REVISION)|g" {} \+

## remove local site files, remove relink file, clean sphinx built files
$(GUIDES.CLEAN):
	make -C $(patsubst %.clean,%, $@) clean
	rm -rf $(DEPLOY_DIR)/guides/$(patsubst %.clean,%, $@)
	rm -rf $(patsubst %.clean,%.relink, $@)

## second pass over local site to get cross-references correct
$(GUIDES.RELINK): | dummyserverstart
	make -C $(patsubst %.relink,%, $@) clean
	make $(patsubst %.relink,%, $@)
	touch $@

.PHONY: help help-prefix help-targets
# ideas from https://gist.github.com/prwhite/8168133

## @helpers Show help
help: | help-prefix help-targets

# COLORS
GREEN  := $(shell tput -Txterm setaf 2)
YELLOW := $(shell tput -Txterm setaf 3)
BLUE   := $(shell tput -Txterm setaf 4)
RED    := $(shell tput -Txterm setaf 5)
WHITE  := $(shell tput -Txterm setaf 7)
RESET  := $(shell tput -Txterm sgr0)

help-prefix:
	@echo 'Usage:'
	@echo '  $(GREEN)make$(RESET) $(YELLOW)<target>$(RESET)'
	@echo '  $(GREEN)make$(RESET) $(YELLOW)<target>$(RESET) $(BLUE)<VAR>$(RESET)=$(RED)<value>$(RESET)'
	@echo ''
	@echo "  $(YELLOW)<target>$(RESET) $(BLUE)is one of those listed below, or the guide specific targets in:$(RESET)"
	@echo "           $(RED)$(GUIDES)$(RESET)"

# --- helper

HELP_TARGET_MAX_CHAR_NUM = 20
help-targets:
	@awk '/^[a-zA-Z\-$()_0-9]+:/ \
		{ \
			helpMessage = match(lastLine, /^## (.*)/); \
			if (helpMessage) { \
				helpCommand = substr($$1, 0, index($$1, ":")-1); \
				helpMessage = substr(lastLine, RSTART + 3, RLENGTH); \
				helpGroup = match(helpMessage, /^@([^ ]*)/); \
				if (helpGroup) { \
					helpGroup = substr(helpMessage, RSTART + 1, index(helpMessage, " ")-2); \
					helpMessage = substr(helpMessage, index(helpMessage, " ")+1); \
				} \
				printf "%s|  $(YELLOW)%-$(HELP_TARGET_MAX_CHAR_NUM)s$(RESET) %s\n", \
					helpGroup, helpCommand, helpMessage; \
			} \
		} \
		{ lastLine = $$0 }' \
		$(MAKEFILE_LIST) \
	| sort -t'|' -sk1,1 \
	| awk -F '|' ' \
			{ \
			cat = $$1; \
			if (cat != lastCat || lastCat == "") { \
				if ( cat == "0" ) { \
					print "Targets:" \
				} else { \
					gsub("_", " ", cat); \
					printf "\nTargets for %s:\n", cat; \
				} \
			} \
			print $$2 \
		} \
		{ lastCat = $$1 }'
