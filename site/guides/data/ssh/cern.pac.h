// PAC file for firefox, linux, etc.,
function FindProxyForURL(url, host) {

  alert("url" + url);

  if (shExpMatch(url,"*.cms:*")                   ||
      shExpMatch(url,"*.cms/*")                   ||
      shExpMatch(url,"*.cmsdaqpreseries:*")       ||
      shExpMatch(url,"*cmsinstallationserver*")   ||
      shExpMatch(url,"*cmsrc-*:*")                ||
      /* shExpMatch(url,"http://cmsicinga2.cms/icinga-web/\*")   || */
      /* shExpMatch(url,"http://cmsicinga2/\*")   || */
      isInNet(host, "172.16.0.0",  "255.255.0.0") ||
      isInNet(host, "10.176.0.0",  "255.255.0.0")
      ) {
    alert("CMS Private Network");
    return "SOCKS5 127.0.0.1:1091";
  } else if (shExpMatch(url,"*.cms904:*") ||
	     shExpMatch(url,"*.cms904/*") ||
	     shExpMatch(url,"*cms904rc-*:*")           ||
	     /* isInNet(host, "172.16.0.0",  "255.255.0.0") || */
	     isInNet(host, "10.192.0.0",  "255.255.0.0")
	     ) {
    alert("CMS904 Private Network");
    return "SOCKS5 127.0.0.1:1081";
  } else if (shExpMatch(url,"*gem904daq01*:*")     || shExpMatch(url,"*gem904daq01.cern.ch:*")  ||
	     shExpMatch(url,"*gem904daq02*:*")     || shExpMatch(url,"*gem904daq02.cern.ch:*")  ||
	     shExpMatch(url,"*gem904daq03*:*")     || shExpMatch(url,"*gem904daq03.cern.ch:*")  ||
	     shExpMatch(url,"*gem904daq04*:*")     || shExpMatch(url,"*gem904daq04.cern.ch:*")  ||
	     shExpMatch(url,"*gem904daq*:*")       || shExpMatch(url,"*gem904daq*.cern.ch:*")   ||
	     shExpMatch(url,"*gem904nas01*")       || shExpMatch(url,"*gem904nas01.cern.ch*")   ||
	     shExpMatch(url,"*gem904qc7daq:*")     || shExpMatch(url,"*gem904qc2daq.cern.ch:*") ||
	     shExpMatch(url,"*gem904qc8daq:*")     || shExpMatch(url,"*gem904qc8daq.cern.ch:*")
	     ) {
    alert("CERN GPN");
    return "SOCKS5 127.0.0.1:1085";
  }

  // when on public wifi
  /* return "SOCKS5 127.0.0.1:1085"; */

  // All other requests go directly to the WWW:
  return "DIRECT";
}
