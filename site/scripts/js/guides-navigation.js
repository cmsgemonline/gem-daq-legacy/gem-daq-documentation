document.onkeydown = function (evt) {
  evt = evt || window.event;
  // console.log($("a[rel]"));
  // console.log("Trying to get prev link");
  // console.log($('a[rel="prev"]'));
  // console.log($('a[rel="prev"]').attr("href"));
  // console.log("Trying to get next link");
  // console.log($('a[rel="next"]'));
  // console.log($('a[rel="next"]').attr("href"));
  var href;
  switch (evt.keyCode) {
    case 37:
      console.log("prev page toggled");
      href = $('a[rel="prev"]').attr("href");
      break;
    case 39:
      console.log("next page toggled");
      href = $('a[rel="next"]').attr("href");
      break;
  }
  if (href) {
    // navigate where the link points
    // console.log(href);
    window.location = href;
  }
};
