.. _gemos-powerone-guide:

===============================
Aspiro PowerOne AC/DC converter
===============================

The GEM μTCA shelves receive their 48V power either via a built-in AC/DC converter (in the case of the Vadatech shelves) or via a dedicated AC/DC converter.
The AC/DC converter used throughout CMS is produced by Aspiro, and is the PowerOne model.
