.. _gemos-ctp7-gem-reg:

===========================
Register CLI advanced usage
===========================

In addition to the overview provided in the :ref:`user guide<userguide:gemos-gem-reg-cli>`, the ``gem_reg.py`` tool allows you to perform additional actions on GEM hardware:


The tool attempts to open an RPC connection to the specified host, thus the ``rpcsvc`` service must be running as ``gemuser`` on that card.
Additionally, the tool parses the address table locally (on the calling PC) by using the ``pickle`` file.
If the pickle file and address table are mismatched, errors can occur (this should not happen if updates are done in the approved way).

An overview of the most common commands is found in the :ref:`user guide<userguide:gemos-cli-commands>`.
All commands are documented in the :py:mod:`xhal API<xhal:gem-reg>`.
A few expert actions are described here for reference

+-----------------+-------------------------------------------------+--------------------------------------+
| Command         | Description                                     | Example usage                        |
+=================+=================================================+======================================+
| ``update_lmdb`` | updates the LMDB on the CTP7 following a        | :ref:`usage<gemos-ctp7-update-lmdb>` |
|                 | breaking change to the register address space   |                                      |
|                 | (address table)                                 |                                      |
+-----------------+-------------------------------------------------+--------------------------------------+


.. _gemos-ctp7-update-lmdb:

Updating the LMDB
-----------------

Whenever the FW of either the CTP7 or the OptoHybrid changes such that a new xml address table is generated (e.g., new node names are added, new addresses are added, or existing nodes (addresses) have their addresses (nodes) altered, FW is compiled for more OptoHybrids) then the LMDB must be updated.
(If the FW update does not include changes to the xml address table then this action does *not* need to be taken).

To update the LMDB launch ``gem_reg.py`` from the *DAQ PC* and after connecting execute:

.. code-block:: bash

   update_lmdb /mnt/persistent/gemdaq/xml/gem_amc_top.xml

.. warning::
   If an error was reported when trying to update the LMDB then it has failed and you must investigate the problem, solve it, and then update the LMDB again.
   A good place to start is in the :ref:`CTP7 log<gemos-ctp7-syslogd>`.

.. note::
   This procedure is automatically run when using the officially supported FW :ref:`update tools<gemos-ctp7-helper-scripts>`
