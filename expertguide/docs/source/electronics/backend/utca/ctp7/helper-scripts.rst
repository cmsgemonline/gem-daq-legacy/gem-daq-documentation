.. _gemos-ctp7-helper-scripts:

=============================
Using the CTP7 helper scripts
=============================

In order to facilitate several common actions performed on the CTP7, several helper scripts have been developed.
In order to use them, you should check out the ``gemctp7user`` repository:

   .. code-block:: bash

      git clone https://github.com/cms-gem-daq-project/gemctp7user.git

To get help about the available options, execute:
   .. code-block:: bash

      ./setup_ctp7.sh -h

From the menu you can choose the appropriate action.
Several common actions are outlined below.

.. _gemos-ctp7-setup-new:

Setting up a *new* CTP7
-----------------------

If you have *just* received a new CTP7, or a new SD card has been placed in an CTP7 already in your possession, you will need to setup the linux partition on the card.
To do this, from the test stand's DAQ PC execute the following:

#. Execute the ``setup_ctp7.sh`` script from the ``gemctp7user`` repo:

   .. code-block:: bash

      cd gemctp7user
      ./setup_ctp7.sh -o X.Y.Z.Q -c A.B.C -l 12 -a -u eagleVV

This will place `OptoHybrid firmware <https://github.com/cms-gem-daq-project/OptoHybridv3/releases>`_ version ``X.Y.Z.Q``, `CTP7 firmware <https://github.com/cms-gem-daq-project/GEM_AMC/releases>`_ version ``A.B.C``, create the ``gemuser`` account, obtain the latest versions of ``xhal`` and ``ctp7_modules``, and transfer all binaries/bit files/xml's/etc., to the approrpriate locations on the CTP7.

.. warning::
   You may find that ``rpcsvc`` may not be running at the time that the ``setup_ctp7.sh`` script tries to update the LMDB.
   This will cause the automatic update of the LMDB to fail.
   This is okay, you can do it manually with the ``gem_reg.py`` CLI (you should ensure that the ``pickle`` file of the address table is correct):

   .. code-block:: bash

      gem_reg.py -n ${ctp7host} -e update_lmdb /mnt/persistent/gemdaq/xml/gem_amc_top.xml


.. _gemos-ctp7-setup-update:

Update CTP7 SW packages
-----------------------

To update only the software packages, e.g., ``xhal`` library, ``ctp7_modules``, or other libraries on the CTP7

#. Execute the ``setup_ctp7.sh`` sxcript from the ``gemctp7user`` repo:

   .. code-block:: bash

      cd gemctp7user
      ./setup_ctp7.sh -u eagleVV


.. _gemos-update-oh-fw-file:

Update OH FW files
------------------

To uptdate the OptoHybrid FW files, e.g., firmware image (bitfile), GBTx generic configurations, and address tables

#. Execute the ``setup_ctp7.sh`` sxcript from the ``gemctp7user`` repo:

   .. code-block:: bash

      cd gemctp7user
      ./setup_ctp7.sh -o X.Y.Z.Q eagleVV


.. _gemos-update-amc-fw-file:

Update AMC FW files
-------------------

To uptdate the AMC (CTP7) FW files, e.g., firmware image (bitfile) and address tables

#. Execute the ``setup_ctp7.sh`` sxcript from the ``gemctp7user`` repo:

   .. code-block:: bash

      cd gemctp7user
      ./setup_ctp7.sh -c A.B.C -l 12 eagleVV


