.. _gemos-electronics-guide:

===========
Electronics
===========

The GEM DAQ system is physically composed of two sets of electronics.

* Back-end electroncis are those present on the detectors themselves.
* Back-end electronics are those present in the off-detector racks.

The link between them is optical.
Information is given in the linked documents on interacting with the different components and debugging various issues.

.. toctree::
   :caption:  Electronics
   :maxdepth: 5

   backend/backend
   frontend/frontend
