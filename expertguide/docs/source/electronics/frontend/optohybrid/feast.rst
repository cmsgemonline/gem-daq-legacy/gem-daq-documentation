.. _gemos-feast-guide:


-------
FEASTMP
-------

The `FEASTMP <https://project-dcdc.web.cern.ch/project-dcdc/public/Documents/FEASTMod_Datasheet.pdf>`_ is a radiation hard DC-DC power converter used by many systems at CERN to convert high input voltage into low output voltage.
The GE1/1 design (both long & short detectors) each use 10 FEASTs:


* F1 1.0V powers FPGA core voltage
* F2 2.55V provides power to VTTx/VTRx,
* F3 1.55V provides power to GBTx and SCA chips,
* F4 1.86V provides power to EPROM (not loaded in OHv3c systems),
* F5 ???
* F6 ???
* FQA, FQB, FQC, FQD, 1.2V providing digital and analog power to 6 VFAT3s each.

Again it is recommended to apply heat sinks to *all* FEASTs and specifically to air cool with a fan F1 and F2.
