.. _gemos-vfat3-guide:

=====
VFAT3
=====

For an in-depth guide on the VFAT3 please consult the `VFAT3 manual <https://espace.cern.ch/cms-project-GEMElectronics/VFAT3/Forms/AllItems.aspx>`_ (CERN SSO login and e-group membership required).

Interacting with the VFAT3 is discussed in :ref:`userguide:gemos-frontend-vfat3` in the users guide.
Currently, there are no additional expert only topics, but should the need arise, they will be placed here.
