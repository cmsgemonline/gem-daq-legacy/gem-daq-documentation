.. _gemos-expert-tcds-guide:

====
TCDS
====


The trigger control and distribution system (TCDS) is responsible for delivering the LHC clock to all subsystems, transporting the TTS states between subsystems and the central DAQ infrastructure, distributing trigger decisions (L1As), and sending TTC B-channel commands (B-Gos).

The `TCDS <https://twiki.cern.ch/twiki/bin/view/CMS/TCDS>`_ system is composed of a number of components, all working together.

+---------------------+----------------+--------------------------+---------------------+
| Component name      | HW component   |  SW component            | Notes               |
+=====================+================+==========================+=====================+
| Partition interface | PI             | PIController             |                     |
+---------------------+----------------+--------------------------+---------------------+
| Integrated TTCci    | iCI            | iCIController            | FW block inside the |
|                     |                |                          | physical LPM        |
+---------------------+----------------+--------------------------+---------------------+
| Local partition     | LPM (iPM)      | LPMController            |                     |
| manager             |                |                          |                     |
+---------------------+----------------+--------------------------+---------------------+
| Central partition   | CPM            | CPMController            | Not controlled      |
| manager             |                |                          | by GEMs             |
+---------------------+----------------+--------------------------+---------------------+


.. 
   :caption: TCDS applications
   :maxdepth: 2

   pi-controller
   ici-controller
   lpm-controller

.. contents:: TCDS appliations
   :local:
   :depth: 2
   :backlinks: entry


.. _gemos-tcds-pi-controller:

PIController
------------



.. _gemos-tcds-lpm-controller:

LPMController
-------------



.. _gemos-tcds-ici-controller:

iCIController
-------------
