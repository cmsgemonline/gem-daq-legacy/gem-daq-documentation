.. _gemos-expert-minidaq-guide:

=======
MiniDAQ
=======

In addition to the standard RCMS FM interface, a special set of applications and infrastructure is provided by the cDAQ group to mimic as closely as possible, cDAQ operations.
This infrastructure is known as "MiniDAQ".
The GEM MiniDAQ configuration by default contains a DAQ function manager and a TCDS function manager.


.. contents:: MiniDAQ topics
   :local:
   :depth: 2
   :backlinks: entry


.. _gemos-minidaq-automator:

Using the automator
-------------------

The "automator" configuration allows you to create a run and get to any state provided all intermediate states transitioned successfully.


.. _gemos-minidaq-add-remove-fed:

Add/remove FED
--------------

To add or remove a FED from the run, use the ``FED&TTS`` button.
Here you must select the FED, whether the DAQ is enabled/disabled, and whether the TTS is enabled/disabled.


.. _gemos-minidaq-random-trigger:

Set random trigger rate
-----------------------

Changing the random trigger rate is done in the ``LPMController`` :ref:`application<gemos-tcds-lpm-controller>`.


.. _gemos-minidaq-hlt-key:

Select CMSSW release (HLT key)
------------------------------

Use the ``HLT`` selector to choose the HLT menu as well as the appropriate CMSSW release to be used.


.. _gemos-minidaq-t0-transfer:

Enable/disable T0 transfer
--------------------------

If the data being taken should be written out to the tier 0 (T0), make sure to set the ``T0_TRANSFER_ON``.
If this is not set at the time of data taking, provided the DAQ FM is not dropping events at the BU, a manual transfer can be requested after the fact, provided not too much time has passed.


.. _gemos-minidaq-stress-test:

MiniDAQ stress-test
-------------------

One of the main uses of MiniDAQ is to emulate the conditions used during cDAQ operations when validating new software and firmware.
Setting up for a "stress-test" involves configuring a normal run.
Several subsequent steps may involve:

* cycling through many start/stop transitions
* setting a high random trigger rate (>100kHz)
* enabling standard CMS B-go sequences (``HardReset``, ``Resync``)

.. note::
   If the random rate is too high, due to the limitations of the hardware availble in the current MiniDAQ, errors may be encountered simply because the disk writing is too slow.
   To isolate issues solely related to the GEM software and firmware, it may be useful to disable the DAQ FM, or tell the DAQ FM to "drop events at BU".
