.. _gemos-expert-rs-manager-guide:

==========
RS manager
==========

The Resource Service Manager (RS Manager) is a tool provided by the CMS RCMS developers to interface with the RS database (RSDB).
This database hosts the RCMS run configurations.


.. contents:: RS manager topics
   :local:
   :depth: 1
   :backlinks: entry


.. _gemos-expert-get-rs-manager:

Obtaining the RS Manager
------------------------

The RSManager application is provided with the RCMS releases.
Releases are announced on the ``hn-cms-run-control`` hypernews, and instructions to download are located on the `CMS run control <http://cmsdoc.cern.ch/cms/TRIDAS/RCMS/Downloads/downloadsIndex.html>`_ website.


.. _gemos-rs-manager-usage:

Using the RS Manager
--------------------

Using the RS manager requires Java.
You should also ensure that you will be able to communicate with the databases the RS manager will attempt to manipulate.
A configuration file can be defined if there are several DBs you routinely work with, and you can switch them on the fly in the tool.


.. _gemos-xdaq-rs-configs:

xDAQ configurations in RSDB
---------------------------

The RCMS team suggest that the subsystem xDAQ executives assign port numbers starting from the base FM port + 100, e.g., at P5, where the ``gempro`` FM runs at ``cms-gempro.cms:20000/rcms``, the first GEM xDAQ executive should be assigned a port ``20100``.
Various RCMS services may be allocated to ports below this, but there should not be any assigned above this.
