.. _gemos-expert-xdaq-guide:

====
xDAQ
====

`xDAQ <xdaq.web.cern.ch/>`_ is a data acquisition framework widely used throughout CMS.

In a complete system there are several components

* ``jobcontrol``: service which is able to stop and start xDAQ applications
* ``XaaS`` : xDAQ as a service
* ``xmas`` : xDAQ monitoring and alarming service
* ``hyperdaq`` : web-based interface to xDAQ applications
* subsystem specific xDAQ applications

How to interface with these components is describe in the linked pages.

.. toctree::
   :maxdepth: 2

   xdaq-services
   gem-xdaq
