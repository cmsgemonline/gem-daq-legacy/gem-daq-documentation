.. _gemos-904-cubicle:

===========================
The 904 cubicle (904-R-U18)
===========================

The 904 cubicle has several desks, PCs, a rack for testing LV and HV supplies, a tool desk, and other ameneties.

.. toctree::
   :hidden:
   :maxdepth: 2

   nas


PCs
---

Two DAQ PCs sit in the cubicle: ``gem904daq02`` and ``gem904daq03``

