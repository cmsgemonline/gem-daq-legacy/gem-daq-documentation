.. _gemos-904-nas:

=========================
The 904 NAS (gem904nas01)
=========================

In the GEM cubicle in 904 (904-R-U18) there is a network attached storage (NAS) unit with 4 slots configured as RAID10.

.. important::
   Currently there are 2TB HDDs installed, resulting in a storage capacity of 4TB, but 4 4TB disks were purchased as an upgrade.
   Neither the disk instllation, nor the RAID rebuild has yet been done.

The NAS serves several purposes:

* Host local RSDB for RCMS in the lab
* Provide central storage certain software packages, e.g., Xilinx tooling
* Provide central storage for lab data
* Provide central user storage
* Provide local YUM repository (obsolete with EOS repo)

The NAS is auto-mounted on all the managed machines in the 904 labs via the ``autofs`` service.
To set this up on a new machine, you should use the :cpp:func:`setupMachine.sh script<gemctp7user:setup_machine>`.
Manual instructions (following the steps executd in the script) are provided below.

.. _gemos-connect-to-nas:

Connecting to the NAS
---------------------

On the machine you are setting up to connect to the NAS, you should cheeck that the ``autofs`` service is running (for ``systemd`` systems)

.. code-block:: sh
   
   systemctl status autofs
   ● autofs.service - Automounts filesystems on demand
   Loaded: loaded (/usr/lib/systemd/system/autofs.service; enabled; vendor preset: disabled)
  Drop-In: /usr/lib/systemd/system/autofs.service.d
           └─50-cvmfs.conf
   Active: active (running) since Mon 2019-12-02 12:06:25 CET; 3 months 22 days ago
 Main PID: 2547 (automount)
    Tasks: 9
   Memory: 109.3M
   CGroup: /system.slice/autofs.service
           └─2547 /usr/sbin/automount --systemd-service --dont-check-daemon

If you do not see that the ``autofs`` service is either running or available, you may have to install it.

.. code-block:: sh

   yum install autofs

Once this is done, you should set up the configuration files:

.. code-block:: sh

   cat <<EOF>/etc/auto.nas
   GEMDAQ_Documentation    -context="system_u:object_r:nfs_t:s0",nosharecache,auto,rw,async,timeo=14,intr,rsize=32768,wsize=32768,tcp,bg,nolock,nosuid,noexec,acl             gem904nas01:/share/gemdata/GEMDAQ_Documentation
   GEM-Data-Taking         -context="system_u:object_r:httpd_sys_content_t:s0",nosharecache,auto,rw,async,timeo=14,intr,rsize=32768,wsize=32768,tcp,bg,nolock,nosuid,noexec,acl gem904nas01:/share/gemdata/GEM-Data-Taking
   sw                      -context="system_u:object_r:nfs_t:s0",nosharecache,auto,rw,async,timeo=14,intr,rsize=32768,wsize=32768,tcp,bg,nolock,nosuid                        gem904nas01:/share/gemdata/sw
   users                   -context="system_u:object_r:nfs_t:s0",nosharecache,auto,rw,async,timeo=14,intr,rsize=32768,wsize=32768,tcp,bg,nolock,nosuid,acl                    gem904nas01:/share/gemdata/users
   +auto.nas
   EOF

   echo '/data/bigdisk   /etc/auto.nas  --timeout=600 --ghost --verbose' > /etc/auto.master

This will result in the 4 NAS mounts being mounted on the local machine at ``/data/bigdis/<mount point>``.

.. warning::
   The NAS hostname ``gem904nas01.cern.ch`` must be resolvable from the machine where this setup is being done.
