.. _gemos-network-topology:

================
Network topology
================

Local network
-------------

For local network setup for 904 labs, and recommended for teststands, several guidelines are provided.
If you use the :cpp:func:`setupMachine.sh script<gemctp7user:setup_machine>`, you will be prompted for how to set up your local network, but it will follow these basic principles:

* μTCA shelves and devices are on a ``192.168.0.0/16`` network.

  * MCHs (NAT, not VadaTech) are assigned IP addresses of ``192.168.<shelf ID>.10``

    .. note::
       The IP address assignment may need to be done manually, if the MCH was not previously configured by one of the GEM DAQ expert team.

  * AMC13s use their hard-coded IP address assignments based on the device S/N

    .. note::
       Assignment via the ``sysmgr`` is possible, but has not been reliably deployed

  * CTP7s in the shelves assigned geographic IP addresses of the form ``192.168.<shelf ID>.<40+slot ID>``
  * Host aliases are set up in ``/etc/hosts`` to allow for name construction in software, e.g., ``gem-shelf01-amc02``
  * GLIBs (and other "emulator-like" devices) are managed through ``docker`` container services, connected to the local μTCA network via a ``macvlan`` interface.


.. _gemos-firewall-rules:

Firewall rules
~~~~~~~~~~~~~~

If using the firewall, several rules should be enabled.
The commands below assume a ``systemd`` based system running ``firewalld`` and configured using ``firewall-cmd``.

.. tip::
   If your network policy allows it, you may also disable ``firewalld``.

   .. code-block:: bash

      systemctl firewalld disable

The rules below assume that **all** μTCA devices are connected on the local.
The rest of the rules depend on whether:

* The CTP7s/AMCs are configured to update software :ref:`via a package manager<gemos-ctp7-smart-rule>`
* The setup :ref:`is running RCMS<gemos-rcms-rule>`
* The setup :ref:`is running xDAQ applications<gemos-xdaq-rule>`
* There is a :ref:`single PC connected to the local network<gemos-control-hub-rule>`, but other machines in the lab need access to the setup hardware.


.. _gemos-local-network-rule:

Local network device rule
^^^^^^^^^^^^^^^^^^^^^^^^^

#. Configure the interface (in this example, the ``enp2s0`` device) that the communicates on the local μTCA network to belong to the ``trusted`` zone

.. code-block:: bash

   sudo firewall-cmd --zone=trusted --add-interface=enp2s0 --permanent
   sudo firewall-cmd --zone=trusted --add-interface=enp2s0

#. Reload the ``firewalld`` service to apply the rules

.. code-block:: bash

   sudo firewall-cmd --reload


.. _gemos-ctp7-smart-rule:

CTP7 smart package manager rule
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

If the CTP7 is configured to pull packages from the ``yum`` repository with the ``smart`` package manager, a connection from the CTP7 on the local netowrk to the site hosting the ``yum`` repository needs to be made.
On the ``sysmgr`` PC execute the following commands (adapted for your specific setup):

* Create port forward from the repository host to the local machine on ports ``10000+<host port>``

.. code-block:: bash

   sudo firewall-cmd --zone=public --add-forward-port=port=10080:proto=tcp:toport=80:toaddr=<repo host IP>  --permanent
   sudo firewall-cmd --zone=public --add-forward-port=port=10443:proto=tcp:toport=443:toaddr=<repo host IP> --permanent
   sudo firewall-cmd --zone=public --add-forward-port=port=10443:proto=tcp:toport=443:toaddr=<repo host IP>
   sudo firewall-cmd --zone=public --add-forward-port=port=10080:proto=tcp:toport=80:toaddr=<repo host IP>

* Create a port forward between the external facing NIC and the local network NIC

.. code-block:: bash

   sudo firewall-cmd --zone=trusted --add-forward-port=port=10080:proto=tcp:toport=10080:toaddr=<local network IP> --permanent
   sudo firewall-cmd --zone=trusted --add-forward-port=port=10443:proto=tcp:toport=10443:toaddr=<local network IP> --permanent
   sudo firewall-cmd --zone=trusted --add-forward-port=port=10443:proto=tcp:toport=10443:toaddr=<local network IP>
   sudo firewall-cmd --zone=trusted --add-forward-port=port=10080:proto=tcp:toport=10080:toaddr=<local network IP>

* Allow the requests coming in on the local netowrk to pass through to the external facing network

.. code-block:: bash

   sudo firewall-cmd --zone=trusted --add-masquerade --permanent
   sudo firewall-cmd --zone=trusted --add-masquerade

* Reload the ``firewalld`` service to apply the rules

.. code-block:: bash

   sudo firewall-cmd --reload


.. _gemos-rcms-rule:

RCMS applications rule
^^^^^^^^^^^^^^^^^^^^^^

The following will allow the RCMS interface to be visible if the machine itself is reachable

.. code-block:: bash

   sudo firewall-cmd --zone=public --add-port=10000/tcp --permanent
   sudo firewall-cmd --zone=public --add-port=10000/tcp

   sudo firewall-cmd --reload


.. _gemos-xdaq-rule:

xdaq applications rule
^^^^^^^^^^^^^^^^^^^^^^

The following will allow the hyperdaq pages of your ``xdaq`` applications to be visible if the machin is reachable.

* ``jobcontrol`` application

.. code-block:: bash

   sudo firewall-cmd --zone=public --add-port=39999/tcp --permanent
   sudo firewall-cmd --zone=public --add-port=39999/tcp

* "Standard" GEM applications

.. code-block:: bash

   sudo firewall-cmd --zone=public --add-port=20000-20600/tcp
   sudo firewall-cmd --zone=public --add-port=20000-20600/tcp --permanent

* As always, reload ``firewalld`` to ensure the rules are updated

.. code-block:: bash

   sudo firewall-cmd --reload


.. _gemos-control-hub-rule:

``control_hub`` machine rule
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The following will allow the requests to the ``control_hub`` (if configured) to succeed.

.. note::
   This shouldn't be necessary, if all devices connected to the local network put that device in the "trusted" zone, as above.

.. code-block:: bash

   sudo firewall-cmd --zone=trusted --add-port=10203/tcp --permanent
   sudo firewall-cmd --zone=trusted --add-port=10203/tcp

In principle, if you have only a ``control_hub`` PC, which routes all traffic to the local μTCA network, and all other DAQ machines are not connected to the local network, you should enable the second set of rules (i.e., in the ``public`` zone).
A network setup such as this would also require a significant number of other changes for CTP7 based, direct connection systems, as you should route **all** traffic to the local network through this PCs NIC.
A setup such as this has not been fully implemented at any test stands, but instructions could be prepared if the need arises.

.. warning::
   This hasn't been verified by the GEM DAQ team, but may be necessary in the case where you have one PC managing the local devices, and other machines not on the local network, but can talk to the PC on its standard NIC.

   .. code-block:: bash

      sudo firewall-cmd --zone=public --add-port=10203/tcp --permanent
      sudo firewall-cmd --zone=public --add-port=10203/tcp

* As always, reload ``firewalld`` to ensure the rules are updated

.. code-block:: bash

   sudo firewall-cmd --reload


.. _gemos-technical-network:

P5/904 technical network
------------------------

The network toplogy for machines and devices connected to the P5 or 904 technical network is managed by the CMS sysadmins.

Physical machines are named by their rack/slot identifiers, virtual machines based on their physical location.

+----------------------+---------------------------+------------------------+
| GEM machine alias    | rack location             | Notes                  |
+======================+===========================+========================+
| ``bridge-s2e01-23``  | ``s2c17-22-01``           | ``control_hub`` and    |
| ``ctrl-s2c17-22-01`` |                           | ``sysmgr`` host        |
+----------------------+---------------------------+------------------------+
| ``gem-dqm01``        | ``s2g18-34-01``           |                        |
+----------------------+---------------------------+------------------------+
| ``gem-dqm01``        | ``s2g18-34-01``           |                        |
+----------------------+---------------------------+------------------------+
| ``gemvm-xdaq15``     | ``kvm-s3562-1-ip151-107`` |                        |
+----------------------+---------------------------+------------------------+
| ``gemvm-v3daq``      | ``kvm-s3562-1-ip151-74``  |                        |
+----------------------+---------------------------+------------------------+


The μTCA shelf identifier is given by the rack U-slot number corresponding to the bottom of the shelf, and all devices therein inherit this base hostname.
For example, currently we have two μTCA shelves installed at P5 in the rack ``S2E01``.
The first shelf was installed with its bottom at ``U23``, and the second was installed with the bottom at ``U14``.
The ``control_hub`` (and consequently, the ``sysmgr``) machine is ``ctrl-s2c17-22-01.cms``, aliased to ``bridge-s2e01-23.cms`` (this is actually the same machine for both μTCA shelves, and there is no corresponding ``bridge-s2e01-14.cms`` alias).
The MCH for this shelf is aliased to ``mch-s2e01-23-01``.
Each AMC13 has two network interfaces, one for the T1 and the other for the T2, and these are aliased to ``amc-s2e01-23-13-t1`` and  ``amc-s2e01-23-13-t2``.
The other AMC cards in the μTCA shelf will have aliases ``amc-s2e01-23-XX``, were ``XX`` is the slot number.

.. note::
   To be consistent with the GEM teststands, additional aliases are added of the form ``gem-shelfXX-amcYY``, and currently the mapping is the follwoing:

   +-----------------+---------------+
   | GEM μTCA shelf  | rack location |
   +=================+===============+
   | ``gem-shelf01`` | ``s2e01-23``  |
   +-----------------+---------------+
   | ``gem-shelf02`` | ``s2e01-14``  |
   +-----------------+---------------+

When registering a new AMC card at P5, a :ref:`ticket<gemos-sysadmin-support>` should be opened with the sysadmins to tell them the "birdname", but the geographic slot-based identifiers should already be registered if the shelf was previously registered.
