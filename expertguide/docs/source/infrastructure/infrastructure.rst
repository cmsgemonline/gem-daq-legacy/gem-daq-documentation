.. _gemos-infra-guide:

==================
DAQ infrastructure
==================

These pages attempt to provide an overview of the various infrastructure items in use by the GEM DAQ project.


.. toctree::
   :caption: Infrastructure
   :maxdepth: 2

   setup
   common
   network-topology
   904/904
   p5/p5
