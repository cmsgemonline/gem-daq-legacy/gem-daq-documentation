.. _gemos-sw-guide:

========
Software
========

The GEM DAQ software is composed of several components.
Code that runs on the control PC, and code that runs on an embedded processor close to the back-end FPGA interface.

.. toctree::

   install/installation
   environment
