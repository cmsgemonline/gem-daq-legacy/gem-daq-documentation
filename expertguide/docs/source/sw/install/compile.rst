.. _gemos-sw-compilation:

===================
Compile from source
===================

Most GEM packages include the ``gembuild`` package as a ``config`` submodule, as it provides a common set of targets and definitions.
The exception is ``cmsgemos``, which loosely inherits from the parent ``xdaq`` build system.

The fully supported building procedure is to build your changes against officially provided RPM installed packages.
It is also possible to build against non-system installed packages, but a few caveats are to be taken into consideration.

#. Choose package to compile, e.g., ``xhal``

.. code-block:: sh

   cd $DEV_AREA
   git clone --recurse-submodules git@github.com:cms-gem-daq-project/xhal.git
   cd xhal

#. Run compilation

.. code-block:: sh

   make -j<n>

#. Prepare an RPM to be installed onto the system you're testing

.. code-block:: sh

   make rpm

#. Make a local installation, against which you may compile a dependent package

.. code-block:: sh

   INSTALL_PREFIX=/path/to/local/install/dirctory make install

#. If the subsequent package you'll be developing requires the ``sysroot`` for cross-compilation, e.g., ``ctp7_modules`` (here ``<target>`` would currently only be ``ctp7``, but in the future would expand)

.. code-block:: sh

   cd /path/to/local/install/dirctory/opt/gem-peta-stage/<target>
   ln -s /opt/gem-peta-stage/<target>/lib .
   ln -s /opt/gem-peta-stage/<target>/usr .
   cd -

#. Check out the dependent package

.. code-block:: sh

   cd $DEV_AREA
   git clone --recurse-submodules git@github.com:cms-gem-daq-project/ctp7_modules.git
   cd ctp7_modules

#. Tell the build system that it should find the cross-compilation dependencies in the local location, as well as the local installation of the dependency (in this case ``xhal``)

.. code-block:: sh

   export PETA_STAGE=/pat/to/local/install/directory/opt/gem-peta-stage
   export XHAL_ROOT=/pat/to/local/install/directory/opt/xhal
   make -j<n>

#. Prepare an RPM to be installed onto the system you're testing

.. code-block:: sh

   make rpm

.. warning::

  For packages that compile for multiple architectures, e.g., ``reedmuller-c``, ``xhal`` and ``ctp7_modules``, it is not recommended to run ``make install`` as a privilegd user, or into a system location, as the non-native tree structure will also be created.
  Provided you are aware of this, you can clean it up manually, or leave it in place.

