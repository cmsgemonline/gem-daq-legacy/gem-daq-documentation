.. _gemos-sw-pip-installation:

=========================
Installation with ``pip``
=========================

The python packages in the gemos ecosystem are also installable via ``pip``.
The legacy software is released to a single repository, and within that structure is a `location <https://cmsgemdaq.web.cern.ch/cmsgemdaq/sw/gemos/repos/releases/legacy/base/tarballs/>`_ for all tarballs.

.. tip::
   To do this, it is recommended to install these packages into an isolated ``virtualenv``.

Locate the appropriate tarball and install using ``pip``, e.g.,

.. code-block:: console

   curl -LO https://cmsgemdaq.web.cern.ch/cmsgemdaq/sw/gemos/repos/releases/legacy/base/tarballs/gempython_vfatqc-2.8.1-final.zip
   pip install -I gempython_vfatqc-2.8.1.zip


.. note::
   Certain GEM python dependencies may be missing, and you should manually install each as needed, as they are not hosted on ``PyPi``.
   The library dependencies can only be installed via ``yum``, or by manually placing the libraries on your system.

   +---------------------------+----------------------------------+------------------------------------+
   | Package name              | GEM python dependencies          | GEM library dependencies           |
   +===========================+==================================+====================================+
   | ``cmsgemos_gempython``    |                                  | ``xhal``                           |
   +---------------------------+----------------------------------+------------------------------------+
   | ``gempython_vfatqc``      | ``cmsgemos_gempython``           |                                    |
   +---------------------------+----------------------------------+------------------------------------+
   | ``gempython_gemplotting`` | ``cmsgemos_gempython``           |                                    |
   +---------------------------+----------------------------------+------------------------------------+
   | ``reg_interface_gem``     | ``reg_interface``                | ``rwreg``, ``xhal``                |
   +---------------------------+----------------------------------+------------------------------------+
   | ``reg_interface``         |                                  | ``rwreg``                          |
   +---------------------------+----------------------------------+------------------------------------+
