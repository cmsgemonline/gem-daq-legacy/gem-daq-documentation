.. _gemos-powercut-guide:

==========
Power Cuts
==========

.. _gemos-powercut-prep:

Preparing for a power cut
-------------------------

In order to safely prepare a GEM teststand for a planned power cut execute:

#. Power down the high voltage.

   * Use the opportunity to power down other sensitive equipment, such as PMT's.

#. Power down the low voltage.
#. Place the µTCA modules in extraction mode:

   * Gently pull the hot swap tab on all AMC's, including the AMC13.
     Wait until the blue LED stays on solid on each AMC's.

   * Gently pull the hot swap tab on the MCH.
     Again, wait until the blue LED stays solid on.

#. Power down the µTCA crate Power Modules one at a time.
   Find the AC/DC converters powering the PM's and turn them off one at a time.
   They can either be built in the crate or external to the crate.
   In the first case, a switch is present on the crate front panel.

#. Finally, power off the DAQ computer.


.. _gemos-powercut-recovery:

Recovering from a power cut
---------------------------

.. note::

   These instructions assume you are working with a system that is setup for v3 electronics.

To recover a GEM teststand after a power cut execute:

#. Ensure the μTCA crate and associated hardware all have power.

   * E.g., the crate, network switches, DAQ computer, etc...

#. Start the DAQ computer first, then:

   * Ensure that the ``sysmgr``, ``xinetd`` and ``dnsmasq`` services are correctly started.
     You can use the ``systemctl status <service name>`` command to check each service status.
     The ``Active`` field must report ``active (running)``.

   * If any of the services is not started, you can start it manually with the following command ``sudo systemctl start <service name>``.

#. Power on the µTCA crate.
   If the power cut was planned, undo the actions executed during :ref:`gemos-powercut-prep`:

   * Power on the µTCA crate Power Modules one at a time.
   * Push the hot swap tab on the MCH and wait for the blue LED to turn off.
   * Push the hot swap tabs on the AMC's, including the AMC13, and wait for the blue LED's to turn off.

#. Enter the AMC13 tool and enable clocks to the AMC of interest by following instructions under :ref:`gemos-amc13-enabling-clock`
#. For each CTP7 login as ``texas`` and execute: ``killall rpcsvc``

   * Right now on boot the CTP7 linux core will start ``rpcsvc`` as the ``texas`` user and this is not gauranteed to have the correct ``$ENV`` for the ``rpcmodules`` on the card.

#. For each CTP7 login as ``gemuser`` and execute the step: ``recover.sh``

   * Check to make sure that all values in the ``GTH Status`` column are ``0x7``.
     If not you will need to :ref:`reload the CTP7 FW<expertguide:gemos-ctp7-reload-fw>` until all ``GTH Status`` column values are ``0x7``.

   * Check to make sure ``rpcsvc`` is running as ``gemuser`` by executing ``ps | grep [r]pcsvc``.
     If ``rpcsvc`` is not running launch it manually as ``gemuser`` by executing: ``rpcsvc``

   * Check to make sure ``ipbus`` is running as ``gemuser`` by executing ``ps | grep [i]pbus``.
     If ``ipbus`` is not running launch it manually as ``gemuser`` by executing: ``ipbus``.

#. For each CTP7 from the DAQ machine try to read the FW address of the CTP7:

   * Execute: ``gem_reg.py``
   * From inside the ``gem_reg.py`` tool execute: ``connect eagleXX`` where ``XX`` is the number of the CTP7 of interest
   * From inside the ``gem_reg.py`` tool execute: ``kw RELEASE`` this should display the FW release of the CTP7, if ``0xdeaddead`` are shown for any entries of the CTP7 registers (e.g., those lines that do ***not** have ``OHX`` in the name for ``X`` some integer) the CTP7 is not programmed correctly.
