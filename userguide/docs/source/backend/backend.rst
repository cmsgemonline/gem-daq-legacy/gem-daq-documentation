.. _gemos-backend-guide:

====================
Back-end Electronics
====================

The GEM back-end electronics system is composed of custom μTCA (and ATCA) FPGA processor cards.
Information is given in the linked documents on interacting with the different components, as well as how to deal with pre and post power cut situations

.. toctree::
   :caption: Back-end Electronics
   :maxdepth: 2

   AMC13      <amc13/amc13>
   CTP7       <ctp7/ctp7>
   Power Cuts <powercut>
