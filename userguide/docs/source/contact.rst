.. _gemos-users-contact:

===========
Contact us!
===========

The majority of communication is done via two Mattermost teams:


* `CMS GEM DAQ team <https://mattermost.web.cern.ch/cms-gem-daq>`_

  * This team has channels that are mainly related to software and firmware development, and infrastructure setup

* `CMS GEM Ops team <https://mattermost.web.cern.ch/cms-gem-ops>`_

  * This team has channels that are mainly dedicated to operational issues with different :ref:`teststands <gemos-teststand-guide>` and CMS P5.

For urgent issues, the GEM DAQ on-call can be reached at (16-2108), but note that support is triaged

#. P5 operations and support
#. 904 QC/testbeam operations and support
#. 904 teststand support
#. Remote teststand support
