.. _gemos-frontend-gbtx:

====
GBTx
====

The `GBTx <http://iopscience.iop.org/article/10.1088/1748-0221/10/03/C03034/meta>`_ is a radiation hard gigabit transceiver for optical links, providing simultaneous transfer of readout data, timing, and trigger signals, as well as slow control and monitoring information.

.. contents:: GBTx information
   :local:
   :depth: 2
   :backlinks: entry

.. _gemos-gbt-elinks:

E-link assignment in GE1/1
--------------------------

A correspondence between the VFAT position identifiers (SW vs. HW), the associated e-link, and the controlling GBTx is shown below.

.. tabs::
   
   .. tab:: GE1/1

      For the GE1/1 OptoHybrid v3 (any version) the correspondence is

      +---------------+---------------+------+---------+
      | VFAT Pos (SW) | VFAT Pos (HW) | GBTx | E-Link  |
      +===============+===============+======+=========+
      | 0             | 24            | 1    | 5       |
      +---------------+---------------+------+---------+
      | 1             | 23            | 1    | 9       |
      +---------------+---------------+------+---------+
      | 2             | 22            | 1    | 2       |
      +---------------+---------------+------+---------+
      | 3             | 21            | 1    | 3       |
      +---------------+---------------+------+---------+
      | 4             | 20            | 1    | 1       |
      +---------------+---------------+------+---------+
      | 5             | 19            | 1    | 8       |
      +---------------+---------------+------+---------+
      | 6             | 18            | 1    | 6       |
      +---------------+---------------+------+---------+
      | 7             | 17            | 0    | 6       |
      +---------------+---------------+------+---------+
      | 8             | 16            | 1    | 4       |
      +---------------+---------------+------+---------+
      | 9             | 15            | 2    | 1       |
      +---------------+---------------+------+---------+
      | 10            | 14            | 2    | 5       |
      +---------------+---------------+------+---------+
      | 11            | 13            | 2    | 4       |
      +---------------+---------------+------+---------+
      | 12            | 12            | 0    | 3       |
      +---------------+---------------+------+---------+
      | 13            | 11            | 0    | 2       |
      +---------------+---------------+------+---------+
      | 14            | 10            | 0    | 1       |
      +---------------+---------------+------+---------+
      | 15            | 9             | 0    | 0       |
      +---------------+---------------+------+---------+
      | 16            | 8             | 1    | 7       |
      +---------------+---------------+------+---------+
      | 17            | 7             | 2    | 8       |
      +---------------+---------------+------+---------+
      | 18            | 6             | 2    | 6       |
      +---------------+---------------+------+---------+
      | 19            | 5             | 2    | 7       |
      +---------------+---------------+------+---------+
      | 20            | 4             | 2    | 2       |
      +---------------+---------------+------+---------+
      | 21            | 3             | 2    | 3       |
      +---------------+---------------+------+---------+
      | 22            | 2             | 2    | 9       |
      +---------------+---------------+------+---------+
      | 23            | 1             | 0    | 8       |
      +---------------+---------------+------+---------+

      .. note::
         GBTx0 doesn't use all its e-links for VFAT communication as it is also responsible for SCA & FPGA communication on the OptoHybrid.


   .. tab:: GE2/1

      For the GE2/1 OptoHybrid v1(v2)

      +---------------+---------------+------+---------+
      | VFAT Pos (SW) | VFAT Pos (HW) | GBTx | E-Link  |
      +===============+===============+======+=========+
      | XX            | YY            | A    | B       |
      +---------------+---------------+------+---------+

   .. tab:: ME0

      For the ME0 OptoHybrid

      +---------------+---------------+------+---------+
      | VFAT Pos (SW) | VFAT Pos (HW) | GBTx | E-Link  |
      +===============+===============+======+=========+
      | XX            | YY            | A    | B       |
      +---------------+---------------+------+---------+


.. _gemos-gbt-phase-scan:

Performing a GBT phase scan
---------------------------

The GBTx must be at least minimally fused (so that it locks to the fiber link) and the ``I2C`` jumper for the GBTx in question must *not* be in place (e.g., open circuit).
Before proceeding please check that the GBTx communication is good by following :ref:`these instructions<gemos-gbt-ready-registers>` to check the GBTx status on a given OH.
Once communication is enabled exectue the following procedure:

.. code-block:: bash

   gbt.py Y X v3b-phase-scan <config file> 2>&1 | tee $HOME/oh_Y_gbt_X_phase_scan.txt

This will scan all phases for all e-links on this GBTx and report whether the phase is good (bad) if the ``SYNC_ERR_CNT`` of the VFAT on that e-link is ``0x1`` (``0x0``).

.. note::
   While the above says ``v3b-phase-scan`` it is good for any v3 OptoHybrid version.

The GBT config files a CTP7 can be found under:

.. code-block:: bash

   /mnt/persistent/gemdaq/gbt


.. _gemos-gbt-manual-phase:

``writeGBTPhase.py``: Manually writing the GBT e-link phase for a given VFAT
----------------------------------------------------------------------------

You can write the GBT e-link phase for a given VFAT or all VFATs using the ``writeGBTPhase.py`` tool by calling from the DAQ machine:

.. code-block:: bash

   writeGBTPhase.py -h

which should produce output:

.. code-block:: bash

   usage: writeGBTPhase.py [-h] {single,all} ...

   Tool for writing GBT phase for a single or all elink

   positional arguments:
     {single,all}  Available subcommands and their descriptions.To view the sub
                   menu call writeGBTPhase.py COMMAND -h e.g.
                   writeGBTPhase.py single -h
       single      write GBT phase for single VFAT
       all         write GBT phase for all VFAT

   optional arguments:
     -h, --help    show this help message and exit

There are two sub-commands ``single`` and ``all``.

.. tabs::

   .. tab:: ``single``

      .. code-block:: bash
      
         $ writeGBTPhase.py single -h
         usage: writeGBTPhase.py single [-h] shelf slot vfat phase link
      
         positional arguments:
           shelf       μTCA shelf number
           slot        AMC slot number in the μTCA shelf
           vfat        VFAT number on the OH
           phase       GBT Phase Value to Write
           link        OH number on the AMC
      
         optional arguments:
           -h, --help  show this help message and exit

      If you want to write the phase for single VFAT:
      
      .. code-block:: bash
      
         writeGBTPhase.py single 1 6 23 7 3
      
      This will write the phase 7 to VFAT23 on ``(shelf,slot,link) = (1,6,3)``.

   .. tab:: ``all``

      .. code-block:: bash
      
         $ writeGBTPhase.py all -h
         usage: writeGBTPhase.py all [-h] shelf slot gbtPhaseFile
      
         positional arguments:
           shelf         μTCA shelf number
           slot          AMC slot number in the μTCA shelf
           gbtPhaseFile  File having link, vfat and phase info.
                         The input file will look like:
                         --------------------------
                         link/i:vfatN/i:GBTPhase/i:
                         4    0    7
                         4    1    9
                         4    2    13
                         --------------------------
      
         optional arguments:
           -h, --help    show this help message and exit

      If you want to write a the phases for all VFATs using the input text file as expected by the script:

      .. code-block:: bash

         writeGBTPhase.py all 1 6 $DATA_PATH/GE11-X-S-INDIA-0015/gbtPhaseSetPoints_GE11-X-S-INDIA-0015_current.log

      Here, we assumed that we are reading the detector ``GE11-X-S-INDIA-0015``.
      This will write phases for all VFATs on ``(shelf, slot) = (1, 6)`` for the link told by the text file.


.. _gemos-gbt-ready-registers:

GBT_READY registers
-------------------

There are a set of registers for each optohybrid in the CTP7 FW that provide information about the GBTx status.
To read these reigsters for the X^th optohybrid from ``gem_reg.py`` execute:

.. code-block:: bash

   kw OH_LINKS.OHX.GBT

For example a healthy set of GBTx chips would have the GBT ready register as ``0x1`` and all the error registers as ``0x0``:

.. code-block:: bash

   eagle60 > kw OH_LINKS.OH1.GBT
   0x65800800 r    GEM_AMC.OH_LINKS.OH1.GBT0_READY                         0x00000001
   0x65800800 r    GEM_AMC.OH_LINKS.OH1.GBT1_READY                         0x00000001
   0x65800800 r    GEM_AMC.OH_LINKS.OH1.GBT2_READY                         0x00000001
   0x65800800 r    GEM_AMC.OH_LINKS.OH1.GBT0_WAS_NOT_READY                 0x00000000
   0x65800800 r    GEM_AMC.OH_LINKS.OH1.GBT1_WAS_NOT_READY                 0x00000000
   0x65800800 r    GEM_AMC.OH_LINKS.OH1.GBT2_WAS_NOT_READY                 0x00000000
   0x65800800 r    GEM_AMC.OH_LINKS.OH1.GBT0_RX_HAD_OVERFLOW               0x00000000
   0x65800800 r    GEM_AMC.OH_LINKS.OH1.GBT1_RX_HAD_OVERFLOW               0x00000000
   0x65800800 r    GEM_AMC.OH_LINKS.OH1.GBT2_RX_HAD_OVERFLOW               0x00000000
   0x65800800 r    GEM_AMC.OH_LINKS.OH1.GBT0_RX_HAD_UNDERFLOW              0x00000000
   0x65800800 r    GEM_AMC.OH_LINKS.OH1.GBT1_RX_HAD_UNDERFLOW              0x00000000
   0x65800800 r    GEM_AMC.OH_LINKS.OH1.GBT2_RX_HAD_UNDERFLOW              0x00000000

.. warning::
   If ``GBTY_READY`` is not ``0x1`` or ``GBTY_WAS_NOT_READY`` stays ``0x1`` after :ref:`gemos-gbt-link-reset` then your communication is probably *not* good.
   Check the following:


   * The electronics are powered,
   * The TX from the CTP7 to the GBTx is going into the left position (OH is oriented with FPGA facing you and VTTx/VTRx are pointing towards the floor) of the VTRx that corresponds to this GBTx, or
   * The TX from the GBTx to the CTP7 makes it to the CTP7 fiber patch panel.

.. _gemos-gbt-link-reset:

Issuing a GBT link reset
------------------------

To reset the GBT links and send the VFAT synchronization command execute:

.. code-block:: bash

   write GEM_AMC.GEM_SYSTEM.CTRL.LINK_RESET 0x1

If your GBTx communication is stable this will reset the following registers to ``0x0``:


* ``GBTY_WAS_NOT_READY``,
* ``GBTY_RX_HAD_OVERFLOW``,
* ``GBTY_RX_HAD_UNDERFLOW``, and
* ``SYNC_ERR_CNT``.

This will be applied to all optohybrids and VFATs on the CTP7.
