.. _gemos-frontend-vfatsync:

=============================
Checking VFAT synchronization
=============================

To check if the VFATs are synchronized on ``OHY`` from ``gem_reg.py`` execute:

.. code-block:: console

   kw SYNC_ERR_CNT Y
   kw CFG_RUN Y

where ``Y`` is an integer representing the OH number.
Any VFAT whose sync error counter is non-zero indicates unstable communication.
In rare cases the sync error counters can all be ``0x0`` but communication may still not be good (i.e., you're on the edge of a good/bad phase for that GBT elink).
This is what the second keyword read (``kw``) is checking, it is a slow control command to the VFATs directly.
If ``0xdeaddead`` is returned for any ``CFG_RUN`` value, this indicates you do not have good communication.

Typical causes of bad communication are:

* The VFAT is not physically present on the hardware
* The GBT phase setting for that e-link is bad
* There is a problem with the hardware (VFAT, OH, or GEB)
