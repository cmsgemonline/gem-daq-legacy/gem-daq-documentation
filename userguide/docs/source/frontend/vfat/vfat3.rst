.. _gemos-frontend-vfat3:

=====
VFAT3
=====

For an in-depth guide on the VFAT3 please consult the VFAT3 Manual available `here <https://espace.cern.ch/cms-project-GEMElectronics/VFAT3/Forms/AllItems.aspx>`_.
The VFAT3 is a much more complicated ASIC than the VFAT2 and requires a little bit more knowledge to successfully use.
While the VFAT3 manual should serve as the end-all-be-all reference on the ASIC, a summary of some generally useful information is provided.

.. toctree::
   :caption:  Additional topics
   :maxdepth: 2

   overview
   dacmonitor
   vfatregs
   vfatsync
   vfatcfgfile
