.. _gemos-frontend-vfatregs:

=======================
Checking VFAT registers
=======================

You can get information on ``VFATY`` of ``OHX`` from ``gem_reg.py`` by executing:

.. code-block:: console

   kw GEM_AMC.OH.OHX.GEB.VFATY.CFG_
   read GEM_AMC.OH.OHX.GEB.VFATY.HW_CHIP_ID

This will print the values of all global registers for this VFAT.

Information about the channel register for channel ``Z`` can be obtained via:

.. code-block:: console

   kw GEM_AMC.OH.OHX.GEB.VFATY.VFAT_CHANNELS.CHANNELZ
