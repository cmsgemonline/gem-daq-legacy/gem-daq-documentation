.. _gemos-frontend-vfat3-overview:

=============================
General overview of the VFAT3
=============================

The VFAT3 has three gain settings of its preamplifier (low, medium, high), two comparator modes ("arming", aka leading-edge, or CFD), several shaping times, two on-ASIC 10-bit ADC's for DAC monitoring, and an internal temperature sensor.

.. important::
   For the chip to function correctly and for all bias currents/voltages to be set properly, the ``CFG_IREF`` value must be set such that the reference current is 10 uA.
   The VFAT3 team has calibration each chip and determined this value, so all that is needed is to use the provided value.
   However, this value is unique per VFAT and care should be taken to ensure it is properly assigned.


.. _gemos-vfat-chip-id:

Chip ID
-------

The VFAT3 also has a hardware e-fuse, which specifies the unique chip ID as a 32-bit integer.
In GE1/1 values 0 to 5000 are expected.

.. note::
   Early prototypes provided this value raw.
   Later it was determined that there was a significant risk of single bit flipping, and the chip ID was encoded before being fused.
   The encoding algorithm is a Reed-Muller (2,5) code, which provides the ability to detect and correct up to three bit flips, as well as detect, but not correct, four bit flips.
   All production VFATs have this value encoded, and the software performs the decoding internally, such that the only time this would be raised to the user level is if an error occurred due to more than three bit flips.


.. _gemos-vfat-gain-settings:

Gain settings
-------------

You can change the gain settings of the preamp by writing the following set of registers:

.. tabs::

   .. tab:: High

      .. code-block:: sh

         CFG_RES_PRE = 1
         CFG_CAP_PRE = 0

   .. tab:: Medium

      .. code-block:: sh

         CFG_RES_PRE = 2
         CFG_CAP_PRE = 1

   .. tab:: Low

      .. code-block:: sh

         CFG_RES_PRE = 4
         CFG_CAP_PRE = 3

It is recommended to use the medium preamp gain setting as it was shown that the high gain setting causes strange behavior due to either saturation, after pulsing, or cross-talk.


.. _gemos-vfat-comp-shape-settings:

Comparator and shaper settings
------------------------------

To switch the comparator modes write the following set of registers:

.. tabs::

   .. tab:: CFD mode

      .. code-block:: sh
      
         CFG_SEL_COMP_MODE = 0
         CFG_FORCE_EN_ZCC = 0

   .. tab:: Arming mode (ARM)

      .. code-block:: sh
      
         CFG_SEL_COMP_MODE = 1
         CFG_FORCE_EN_ZCC = 0

It is recommended to use the comparator in CFD mode.
If using the comparator in CFD mode, the shaping time should be set to the maximum to try to integrate the full pulse charge for the CFD technique.
If the comparator is used in arming mode, the shaping time should be set to the minimum to trigger the comparator as fast as possible (when pulse is over threshold).
These two can be accomplished via:

.. tabs::

   .. tab:: CFD mode

      .. code-block:: sh
      
         CFG_FP_FE = 0x7
         CFG_PT = 0xf

   .. tab:: Arming mode

      .. code-block:: sh
      
         CFG_FP_FE = 0x0
         CFG_PT = 0x1


.. _gemos-vfat-calib-settings:

Calibration modules settings
----------------------------

The calibration module can inject charge either in current injection of voltage step pulsing.
Both modes use the same circuit but are compliments of one and other (e.g., high current injection is low voltage step).
From s-curve results in the lab we have not see a difference between these two modes and voltage step pulsing is typically used by default.

Both the comparator and the calibration module can be configured to look at (inject) either positive or negative polarity pulses.
For calibration scans to be effective the polarity of the calibration module must match the polarity expected by the comparator.
Additionally during data taking the polarity the comparator should match the polarity of the GEM signal (i.e., negative polarity).
To ensure there are no mistakes both polarities are set such that:

.. code-block:: sh

   CFG_SEL_POL = 0x0
   CFG_CAL_SEL_POL = 0x0


.. _gemos-vfat-dac-mon-settings:

DAC monitoring
--------------

To ensure proper temperature reading and any DAC monitoring the ``CFG_VREF_ADC`` must be set such that this is as close to 1.0V as possible (again provided by VFAT3 team at production time).
The ``HV3b_V2`` hybrids only have the internal temperature sensor on the VFAT3 ASIC while ``HV3b_V3`` and ``V4`` hybrids have an external PT100 sensor for monitoring temperature.

The comparator has two voltage DAC registers for specifying the voltage on the comparator (note this should *not* be confused with the threshold, which is the 50% point on an s-curve, i.e., where the channel responds to charge 50% of the time at fixed comparator voltage setting).
One value is the ``CFG_THR_ARM_DAC`` and the other is the ``CFG_THR_ZCC_DAC``.
The later must be calibrated for proper channel trimming; the former is of little concern since this is not used in either arming or CFD comparator modes.
For more details on comparator voltage settings see the VFAT3 manual.

Finally there are a few calibration coefficients that are needed:

* ``CAL_DACM``, slope in ``y=mx+b`` for converting ``CFG_CAL_DAC`` to ``fC``,
* ``CAL_DACB``, as ``CAL_DACM`` but for intercept,
* ``ADC0M``, slope in ``y=mx+b`` for converting ADC counts to ``mV`` for ADC0,
* ``ADC0B``, as ``ADC0M`` but for intercept,
* ``ADC1M``, as ``ADC0M`` but for ``ADC1``,
* ``ADC1B``, as ``ADC0B`` but for ``ADC1``,
* ``CAL_THR_ARM_DAC_M``, slope in ``y=mx+b`` for converting ``CFG_THR_ARM_DAC`` to ``fC`` (needed for trimming),
* ``CAL_THR_ARM_DAC_B``, as ``CAL_THR_ARM_DAC_M`` but for intercept.
