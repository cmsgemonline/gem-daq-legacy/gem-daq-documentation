.. _gemos-frontend-guide:

=====================
Front-end Electronics
=====================

Front-end electronics are those present on the detector itself.
The primary component is the :ref:`VFAT3<gemos-frontend-vfat3>` chip, which translates the detector readout signals into hit information.
The :ref:`OptoHybrid<gemos-frontend-optohybrid>` links the back-end electronics with the detector via custom designed, radiation hard, optical links (VTTx/VTRx) and custom radiation hard ASICs (:ref:`GBTx<gemos-frontend-gbtx>` and :ref:`SCA<gemos-frontend-sca>`).
Power is distributed to the on-detector components via a custom DC-DC converter, the :ref:`FEASTMP<gemos-frontend-feast>`.
The documents linked below will provide you with a brief overview of the system so that you can operate.
For more detailed information, you will find more details in the :ref:`corresponding seciton in the expert guide<expertguide:gemos-frontend-guide>`


.. toctree::
   :caption: Front-end Electronics
   :maxdepth: 2

   lvpower
   OptoHybrid <optohybrid/optohybrid>
   VFAT3      <vfat/vfat3>
